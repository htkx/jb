package job;

import job.domain.Department;
import job.domain.DepartmentRepository;
import job.domain.User;
import job.domain.UserRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class JobApplication {

//    @Bean
//    public InitializingBean populate(UserRepository userRepository,
//                                     DepartmentRepository departmentRepository){
//        return () -> {
//
//            Department test = departmentRepository.save(new Department("TEST"));
//
//            Arrays.asList("Bob,Smith,John".split(",")).forEach(name ->
//                    userRepository.save(new User(name, test))
//            );
//
//        };
//    }

    public static void main(String[] args) {
        SpringApplication.run(JobApplication.class, args);
    }
}
