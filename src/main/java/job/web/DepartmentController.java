package job.web;

import job.domain.Department;
import job.domain.User;
import job.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

/**
 * Created by denis on 13/04/15.
 */

@RestController
@RequestMapping(value = "/departments")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

//    @InitBinder
//    public void disallowFields(WebDataBinder dataBinder) {
//        dataBinder.setDisallowedFields("id");
//    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Department> findAllDepartments(){
        return departmentService.findAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addDepartment(@RequestBody Department department) {
        Department result = departmentService.save(department);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getId()).toUri());

        return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Department findDepartmentById(@PathVariable("id") int id) {
        return departmentService.findOne(id).orElseThrow( () -> new DepartmentNotFoundException(id));
    }

    @RequestMapping(value = "/{id}/add/", method = RequestMethod.POST)
    public ResponseEntity<?> addUserToDepartment(@PathVariable("id") int id,
                                                 @RequestBody User user) {
        Department department = departmentService.findOne(id).orElseThrow(() -> new DepartmentNotFoundException(id));

        department.addUser(user);
        departmentService.save(department);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(department.getId()).toUri());

        return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
    }

}
