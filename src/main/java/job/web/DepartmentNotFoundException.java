package job.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
class DepartmentNotFoundException extends RuntimeException {

    public DepartmentNotFoundException(int id) {
        super("could not find department '" + id + "'.");
    }
}