package job.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by denis on 13/04/15.
 */

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
