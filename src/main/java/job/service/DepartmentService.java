package job.service;

import job.domain.Department;
import job.domain.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

/**
 * Created by denis on 13/04/15.
 */

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

    public Department save(Department department) {
        return departmentRepository.save(department);
    }

    public Optional<Department> findOne(int id) {
        return Optional.ofNullable(departmentRepository.findOne(id));
    }

}
