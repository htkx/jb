package job.service;

import job.domain.User;
import job.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by denis on 13/04/15.
 */

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public Optional<User> findOne(int id) {
        return Optional.ofNullable(userRepository.findOne(id));
    }

    public void delete(int id) {
        userRepository.delete(id);
    }
}
