package job;

import job.domain.Department;
import job.domain.DepartmentRepository;
import job.domain.User;
import job.domain.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.converter.json.Jackson2ObjectMapperBuilder.json;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JobApplication.class)
@WebAppConfiguration
public class JobApplicationTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	private MockMvc mockMvc;

	private User user;
	private Department department;
	private List<User> users = new ArrayList<>();

	@Autowired
	UserRepository userRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
				hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

		Assert.assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		this.userRepository.deleteAllInBatch();
		this.departmentRepository.deleteAllInBatch();

		this.department = departmentRepository.save(new Department("IT"));
		this.users.add(userRepository.save(new User("Bob" ,department)));
		this.users.add(userRepository.save(new User("Smith", department)));
	}

	@Test
	public void userNotFound() throws Exception {
		mockMvc.perform(get("/users/11")
				.contentType(contentType))
				.andExpect(status().isNotFound());
	}

	@Test
	public void addUserToDepartment() throws Exception {
		String userJson = json(new User("Bill", department));
		this.mockMvc.perform(post("/departments/" + this.department.getId() + "/add")
				.content(userJson))
				.andExpect(status().isOk());

	}

	@Test
	public void readSingleUser() throws Exception{
		mockMvc.perform(get("/users/" + this.users.get(0).getId()))
				.andExpect(jsonPath("$.id", is(this.users.get(0).getId())))
				.andExpect(jsonPath("$.name", is(this.users.get(0).getName())));

	}

	@Test
	public void creteUser() throws Exception {
		String userJson = json(new User("Sue"));
		this.mockMvc.perform(post("/users/add")
				.contentType(contentType)
				.content(userJson))
				.andExpect(status().isCreated());
	}

	public String json(Object obj) throws IOException {
		MockHttpOutputMessage mockHTTPOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(obj, MediaType.APPLICATION_JSON, mockHTTPOutputMessage);
		return mockHTTPOutputMessage.getBodyAsString();
	}

}
